﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmAssignment1
{
    class SavingsAccount
    {
        float MATCHING_DEPOSIT_RATIO = 0.5F;
        float MIN_INTEREST_RATE = 3.0F;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="acctNo">account number</param>
        /// <param name="acctHolderName">account holder's name</param>
        public SavingsAccount(byte acctNo, string acctHolderName)
        {

        }

        /// <summary>
        /// Change the annual interest rate on the account
        /// </summary>
        /// <param name="newAnnualIntrRatePercentage">the annual interest rate</param>
        public void setAnnualIntrRate(float newAnnualIntrRatePercentage)
        {

        }

        /// <summary>
        /// dposits the given ammount in the account
        /// </summary>
        /// <param name="amount">amount to be deposited</param>
        public void deposit(float amount)
        {

        }
    }
}
