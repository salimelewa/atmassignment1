﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmAssignment1
{

    class Atm
    {
        /// <summary>
        /// The bank this ATM object is working with
        /// </summary>
        private Bank _bank;
        
        /// <summary>
        /// creating the main menu options
        /// </summary>
        private int SELECT_ACCOUNT_OPTION;
        private int CREATE_ACCOUNT_OPTION;
        private int EXIT_ATM_APPLICATION_OPTION;
        
        /// <summary>
        /// creating the account menu option
        /// </summary>
        private int CHECK_BALANCE_OPTION;
        private int WITHDRAW_OPTION;
        private int DEPOSIT_OPTION;
        private int EXIT_ACCOUNT_OPTION;

        /// <summary>
        /// creating an instance of each option in a constructor
        /// </summary>
        public Atm(Bank bank)
        {
            _bank = bank;
            SELECT_ACCOUNT_OPTION = 1;
            CREATE_ACCOUNT_OPTION = 2;
            EXIT_ATM_APPLICATION_OPTION = 3;
            CHECK_BALANCE_OPTION = 1;
            WITHDRAW_OPTION = 2;
            DEPOSIT_OPTION = 3;
            EXIT_ACCOUNT_OPTION = 4;

        }

        /// <summary>
        ///  Starts the ATM program by displaying the required user options. 
        ///  User navigates the menus managing their accounts
        /// </summary>
        public void start()
        {
            int selectedOption;
            //ChequingAccount,SavingsAccount, Account acct;
            //I know this is not the right way to create an instance but
            //I did not know how to create it for multiple methods at the same time
            //this is only applied here not for the others
            //did not want to continue putting mistakes.
            //What I did later on is everytime its more than one type of instance
            //I just put it as Account <variable name>.



        }
        /// <summary>
        ///Displays the main ATM menu and ensure the user picks an option. Handles invalid input but doesn't check
        ///that the menu option is one of the displayed ones.
        /// </summary>
        /// <returns>the option selected by the user</returns>
        public byte ShowMainMenu()
    {
            return 0;
    }
        /// <summary>
        /// Displays the ACCOUNT menu that allows the user to perform account operations. Handles invalid input but doesn't check
        /// that the menu option is one of the displayed ones.
        /// </summary>
        /// <returns> the option selected by the user</returns>
        public byte ShowAccountMenu()
        {
            return 0; 
        }
        ///<summary>
        /// Create and open an account. The user is prompted for all account information including the type of account to open.
        /// Create the account object and add it to the bank
        ///</summary>
        public void onCreateAccount()
        {
            string clientName;
            float initDepositAmount;
            float annIntrRate;
            byte acctType;  
        }

        ///<summary>
        ///Select an account by prompting the user for an account number and remembering which account was selected.
        ///Prompt the user for performing account information such deposit and withdrawals
        ///</summary>
        public void selectAccount()
        {
            string acctNoInput;
            int acctNo;
        }

        ///<summary>
        ///Manage the account by allowing the user to execute operation on the given account
        ///</summary>
        public void manageAccount(Account account)
        {
            byte selAcctMenuOpt;   
        }

        ///<summary>
        ///Prompts the user to enter the name of the client and allows the user to cancel by pressing ENTER
        ///</summary>
        ///<returns>ClientName</returns>
        public string promptForClientName()
        {
            string clientName;
            return null;
        }

        /// <summary>
        ///Prompts the user to enter an account balance and performs basic error checking
        ///</summary>
        ///<returns>Initial Amount</returns>
        public float promptForDepositAmount()
        {
            float initAmount;
            return 0;
            
        }

        ///<summary>
        ///Prompts the user to enter the annual interest rate for an account
        ///</summary>
        ///<returns>intrest rate</returns>
        public float promptForAnnualIntrRate()
        {
            float initRate;
            return 0;
        }

        ///<summary>
        ///Prompts the user to enter an account type
        ///</summary>
        ///<returns>The account type</returns>
        public string promptForAccountType()
        {
            string acctTypeInput;
            return null;  
        }

        ///<summary>
        ///Prints the balance in the given account
        ///</summary>
        public void onCheckBalance(Account account)
        {
            
        }
        ///<summary>
        ///Prompts the user for an amount and performs the deposit. Handles any errors related to incorrect amounts
        ///</summary>
        public void onDeposit(Account account)
        {
            string inputAmount;
            float amount;
           
        }
        ///<summary>
       ///lets the user withdraw money from the account 
      ///has an option if the user wants to cancel proccess
     ///preforms basic error checking as well as invalid transcation
    ///</summary>
        public void onWithdraw(Account account)
        {
            string inputAmount;
            float amount;

        }

    }
}
