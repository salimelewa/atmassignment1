﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmAssignment1
{
    class ChecquingAccount
    {
        private int OVERDRAFT_LIMIT = 500;
        private float MAX_INTEREST_RATE = 1.0F;

        /// <summary>
        /// constructor 
        /// </summary>
        /// <param name="accotNo">account number</param>
        /// <param name="acctHolderName">account holder's name</param>
        public ChecquingAccount(byte accotNo, string acctHolderName)
        {

        }

        /// <summary>
        /// Change the annual interest rate on the account
        /// </summary>
        /// <param name="newAnnualIntrRatePercentage">the annual interest rate</param>
        public void setAnnualIntrRate(float newAnnualIntrRatePercentage)
        {

        }

        /// <summary>
        /// Withdraw the given amount from the account
        /// </summary>
        /// <param name="amount">the amount to be withdrawen cannot be negative or greater than the amount in the account</param>
        /// <returns>the new balance after the withdraw</returns>
        public float withdraw(float amount)
        {
            float oldBalance;
            //oldBalance = Account._balance;
            //_balance -= amount;
            //return _balance;
            return 0;

        }
    }
}
