﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmAssignment1
{
    class Bank
    {
        private List<Account> _accountList;
        byte DEFAULT_ACCT_NO_START;

        /// <summary>
        /// Initialize the field variables of the bank object, the account of the bank
        /// </summary>
        public Bank()
        {
            _accountList = new List<Account>();
            DEFAULT_ACCT_NO_START = 100;

        }

        /// <summary>
        /// Load the account data for all the accounts. The account data files are stored in a directory
        ///  named BankingData located in the current directory, the directory used to run the application from
        /// </summary>
        public void loadAccountData()
        {
            object dataDirectory;
            List<object> acctFileList;
            SavingsAccount acct;

        }

        /// <summary>
        /// Saves the data for all accounts in the data directory of the application. Each account is
        //saved in a separate file which contains all the account information.The account data files are stored in a
       ///directory named BankingData located in the current directory, the directory used to run the application from
        /// </summary>
        public void saveAccountData()
        {
            object dataDirectory;
            Account acct;
            string acctType;
            string prefix;
            string acctFileName;
        }
        /// <summary>
        /// "Create 10 accounts with predefined IDs and balances. The default accounts are created only
        ///if no account data files exist
        /// </summary>
        public void createDefaultAccounts()
        {
            byte iAccount;
            Account newDefAcct;
        }
        /// <summary>
        ///  Returns the account with the given account number or null if no account with that ID can be found
        /// </summary>
        /// <param name="acctNo"> the account number of the account to return</param>
        /// <returns>the account object with the given ID</returns>
        public byte findAccount(byte acctNo)
        {
            Account acct;
            return 0;
        }

        /// <summary>
        /// Determine the account number prompting the user until they enter the correct information
        ///The method will raise an AssertError if the user chooses to terminate.
        /// </summary>
        public void determineAccountNumber()
        {
            string acctNoInput;
            byte acctNo;
            Account account;

        }

        /// <summary>
        /// Create and store an account objec with the required attributes
        /// </summary>
        /// <param name="clientName">Clients' name</param>
        /// <param name="acctType">Account type</param>
        public Account openAccount(string clientName, byte acctType)
        {
            byte acctNo;
            Account newAccount;
            //return newAccount;
            return null;
        }
    }
}
