﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtmAssignment1
{
    /// <summary>
    /// Defines a bank account its associated attributes and operations.
    ///Attributes:
   ///  _acctNo         : int   -- the account number, read-only attribute
   ///  _acctHolderName : str   -- the name of the account holder, read-only attribute
  ///  _balance        : float -- the account balance that gets affected by withdrawls and deposits
  /// _annualIntrRate : float -- the annual interest rate applicable on the balance
    /// </summary>
    class Account
    {
        /// <summary>
        /// these are class variables which are not specific to an instance, they are not field variables
        /// they are shared by all instances and are accessible using the name of the class with the DOT notation
        /// </summary>
        byte ACCOUNT_TYPE_CHECQUING = 1;
        byte ACCOUNT_TYPE_SAVINGS = 2;

        private int _acctNo;
        private string _acctHolderName;
        private float _balance;
        private float _annualIntrRate;

        /// <summary>
        /// Initialize the account object with its attributes.
        ///The account constructor requires the caller to supply an account number and
        ///the name of the account holder in order to create an account.
        /// </summary>
        /// <param name="acctNo"> account number</param>
        /// <param name="acctHolderName">account holder name</param>
        public Account(int acctNo, string acctHolderName)
        {
            _acctNo = acctNo;
            _acctHolderName = acctHolderName;
            _balance = 0.0F;
            _annualIntrRate = 0.0F;
        }

        /// <summary>
        /// Return the account number
        /// </summary>
        public int getAccountNumber()
        {
            return _acctNo;
        }
        
        /// <summary>
        /// Return the account holder's name
        /// </summary>
        public string getAcctHolderName()
        {
            return _acctHolderName;
        }

        /// <summary>
        /// Return the balance in the account
        /// </summary>
        public float getBalance()
        {
            return _balance;
        }

        /// <summary>
        /// Return the Annual interest rate on the account
        /// </summary>
        /// <returns></returns>
        public float getAnnualIntrRate()
        {
            return _annualIntrRate;
        }

        /// <summary>
        /// Change the annual interest rate on the account
        /// </summary>
        /// <param name="newAnnualIntrRatePercentage">the annual interesst as a percentage</param>
        public void setAnnualIntrRate(float newAnnualIntrRatePercentage)
        {
            _annualIntrRate = newAnnualIntrRatePercentage / 100;

        }

        /// <summary>
        /// Calculate the monthly interest rate
        /// </summary>
        /// <returns>The monthly interest rate on the account</returns>
        public float getMonthlyIntrRate()
        {
            return _annualIntrRate / 12;
        }

        /// <summary>
        /// Deposit the given amount in the account
        /// </summary>
        /// <param name="amount">the amount to be deposited</param>
        /// <returns> the new balance after the amount was deposited</returns>
        public float deposit(float amount)
        {
            float oldBalance;
            oldBalance = _balance;
            _balance += amount;
            return _balance;

        }

        /// <summary>
        /// Withdraw the given amount from the account
        /// </summary>
        /// <param name="amount">the amount to be withdrawen cannot be negative or greater than the amount in the account</param>
        /// <returns>the new balance after the withdraw</returns>
        public float withdraw(float amount)
        {
            float oldBalance;
            oldBalance = _balance;
            _balance -= amount;
            return _balance;

        }

        /// <summary>
        /// Load the account infromation from the given file
        /// </summary>
        /// <param name="file">the file containing the account information</param>
        public void load(string file)
        {

        }

        /// <summary>
        /// save the account information in the given file
        /// </summary>
        /// <param name="file">the file to contain the account information</param>
        public void save(string file)
        {

        }
    }
}
